module StudentsHelper

  def sex(s)
    result = ""
    if s
      result = "Homme" if s.upcase=='M'
      result = "Femme" if s.upcase=='F'
    end
    return result
  end

end
