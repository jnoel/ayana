module ApplicationHelper

  def month_array
    return [t("january"),t("february"),t("march"),t("april"),t("may"),t("june"),t("july"),t("august"),t("septembre"),t("october"),t("november"),t("december")]
  end

  def month_array_min
    return [t("jan"),t("feb"),t("mar"),t("apr"),t("may"),t("jun"),t("jul"),t("aug"),t("sep"),t("oct"),t("nov"),t("dec")]
  end

  def month_array_min_english
    return ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"]
  end

end
