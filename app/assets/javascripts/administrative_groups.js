$(function () {
  $button = $('#button');
  $button.click(function () {

    var svg  = document.getElementById('graph');
    var xml  = new XMLSerializer().serializeToString(svg);
    var data = "data:image/svg+xml;base64," + btoa(xml);
    var img  = new Image();

    img.setAttribute('src', data);
    document.body.appendChild(img);

    //window.location = img.src;
  });
});
