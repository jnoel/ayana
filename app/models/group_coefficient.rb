class GroupCoefficient < ActiveRecord::Base
  belongs_to :administrative_group
  validates :year, :administrative_group_id, :coefficient, :presence => true
end
