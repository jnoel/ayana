class Caisse < ActiveRecord::Base
  validates :title, presence: true
  validates :title, uniqueness: true
  has_many :students
end
