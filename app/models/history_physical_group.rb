class HistoryPhysicalGroup < ActiveRecord::Base
  belongs_to :student
  belongs_to :physical_group
  validates :student_id, :physical_group_id, :date_in, :presence => true

  before_create {
    last_history = self.student.last_physical_group
    if last_history
      last_history.date_out = self.date_in-1
      last_history.save
    end
  }

  after_create {
    self.student.physical_group_id = self.physical_group_id
    self.student.save
  }
end
