class AdministrativeGroup < ActiveRecord::Base
	include ApplicationHelper
	has_many :absences
	has_many :students
	has_many :group_coefficients
	validates :title, :presence => true

	def number_of_students
		Student.where(administrative_group_id: self.id).count
	end

	def students_at(date)
    students = Student.joins(:history_administrative_groups)
                      .where("history_administrative_groups.administrative_group_id=? AND history_administrative_groups.date_in <= ? AND (history_administrative_groups.date_out >= ? OR history_administrative_groups.date_out IS NULL)",
                              self.id, date, date)
  end

	def nb_students_at(date)
		students_at(date).count
	end

	def nb_absences_at(month, year)
		date_deb = Date.parse("#{year}-#{month}-01")
		date_fin = date_deb + 1.month - 1.day
		nb = 0.0
		Absence.where(administrative_group_id: self.id, date: date_deb..date_fin, absence: true).each do |absence|
			if absence.morning and absence.afternoon
				nb += 1
			elsif absence.morning or absence.afternoon
				nb += 0.5 # Une absence de demi-journée est comptée comme entière ou moitié
			end
		end
		return nb
	end

	def nb_jours_prevus(date)
    open_days = OpenDay.where(year: date.year)
    nb_open_days = open_days.count>0 ? open_days.first[month_array_min_english[date.month-1]] : 0
    nb = 0
    students_at(date).each do |student|
      nb += nb_open_days * (student.coef.to_f>0.0 ? student.coef : self.coeff(date.year))
    end
    return nb
  end

	def coeff(year)
		return 0.9
	end

end
