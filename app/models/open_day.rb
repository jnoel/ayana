class OpenDay < ActiveRecord::Base
  validates :year, :jan, :feb, :mar, :apr, :may, :jun, :jul, :aug, :sep, :oct, :nov, :dec, :presence => true
  validates :year, uniqueness: true
end
