class Absence < ActiveRecord::Base
  validates :student, :physical_group,:administrative_group, :date, :reason, :presence => true
  belongs_to :student
  belongs_to :physical_group
  belongs_to :administrative_group
  belongs_to :reason

  def hour
    array = []
    array << "Matin" if self.morning
    array << "Après-midi" if self.afternoon
    array << "Nuit" if self.night
    return array.join("-")
  end
end
