class HistoryAdministrativeGroup < ActiveRecord::Base
  belongs_to :student
  belongs_to :administrative_group
  validates :student_id, :administrative_group_id, :date_in, :presence => true

  before_create {
    last_history = self.student.last_administrative_group
    if last_history
      last_history.date_out = self.date_in-1
      last_history.save
    end
  }

  after_create {
    self.student.administrative_group_id = self.administrative_group_id
    self.student.save
  }
end
