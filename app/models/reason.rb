class Reason < ActiveRecord::Base
	has_many :absences
	validates :title, :presence => true
end
