class Student < ActiveRecord::Base
  belongs_to :physical_group
  belongs_to :administrative_group
  belongs_to :caisse
  has_many :absences
  has_many :history_physical_groups
  has_many :history_administrative_groups
  validates :last_name, :first_name, :physical_group_id, :administrative_group_id, :presence => true

  after_create {
    HistoryPhysicalGroup.create student_id: self.id, physical_group_id: self.physical_group_id, date_in: self.date_in
    HistoryAdministrativeGroup.create student_id: self.id, administrative_group_id: self.administrative_group_id, date_in: self.date_in
  }

  def age
  	now = Time.now.utc.to_date
    if self.birth_date
      dob = self.birth_date
    	return now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
    else
      return nil
    end
  end

  def name
    return "#{self.last_name.upcase} #{self.first_name.split.map(&:capitalize).join(' ')}"
  end

  def absences_by_month(month, year, administrative_group_id=nil, physical_group_id=nil)
    start_date = Date.parse("01/#{month}/#{year}")
    end_date = start_date + 1.month - 1.day
    nb = 0
    if administrative_group_id
      nb = Absence.where("date >= ? and date <= ? and student_id = ? and administrative_group_id = ? and absence = ?",
                        start_date,
                        end_date,
                        self.id,
                        administrative_group_id,
                        true).count
    elsif physical_group_id
      nb = Absence.where("date >= ? and date <= ? and student_id = ? and physical_group_id = ? and absence = ?",
                        start_date,
                        end_date,
                        self.id,
                        physical_group_id,
                        true).count
    else
      nb = Absence.where("date >= ? and date <= ? and student_id = ? and absence = ?",
                        start_date,
                        end_date,
                        self.id,
                        true).count
    end
    return nb
  end

  def last_physical_group
    return HistoryPhysicalGroup.where(student_id: self.id).order(:date_in).last
  end

  def last_administrative_group
    return HistoryAdministrativeGroup.where(student_id: self.id).order(:date_in).last
  end

  def physical_group_at(date)
    return HistoryPhysicalGroup.where(student_id: self.id, physical_group_id: self.physical_group_id).where("date_in <= ? AND (date_out >= ? OR date_out IS NULL)", date, date).order(:date_in).last
  end

  def administrative_group_at(date)
    return HistoryAdministrativeGroup.where(student_id: self.id, administrative_group_id: self.administrative_group_id).where("date_in <= ? AND (date_out >= ? OR date_out IS NULL)", date, date).order(:date_in).last
  end
end
