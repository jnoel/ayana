class GroupCoefficientsController < ApplicationController
  before_action :set_group_coefficient, only: [:show, :edit, :update, :destroy]

  # GET /group_coefficients
  # GET /group_coefficients.json
  def index
    @group_coefficients = GroupCoefficient.all
  end

  # GET /group_coefficients/1
  # GET /group_coefficients/1.json
  def show
  end

  # GET /group_coefficients/new
  def new
    @group_coefficient = GroupCoefficient.new
    @administrative_groups = AdministrativeGroup.all
    @group_coefficient.year = params["year"] ? params["year"].to_i : Date.today.year
  end

  # GET /group_coefficients/1/edit
  def edit
    @administrative_groups = AdministrativeGroup.all
  end

  # POST /group_coefficients
  # POST /group_coefficients.json
  def create
    @group_coefficient = GroupCoefficient.new(group_coefficient_params)

    respond_to do |format|
      if @group_coefficient.save
        format.html { redirect_to @group_coefficient, notice: 'Group coefficient was successfully created.' }
        format.json { render :show, status: :created, location: @group_coefficient }
      else
        format.html { render :new }
        format.json { render json: @group_coefficient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /group_coefficients/1
  # PATCH/PUT /group_coefficients/1.json
  def update
    respond_to do |format|
      if @group_coefficient.update(group_coefficient_params)
        format.html { redirect_to @group_coefficient, notice: 'Group coefficient was successfully updated.' }
        format.json { render :show, status: :ok, location: @group_coefficient }
      else
        format.html { render :edit }
        format.json { render json: @group_coefficient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /group_coefficients/1
  # DELETE /group_coefficients/1.json
  def destroy
    @group_coefficient.destroy
    respond_to do |format|
      format.html { redirect_to group_coefficients_url, notice: 'Group coefficient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group_coefficient
      @group_coefficient = GroupCoefficient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_coefficient_params
      params.require(:group_coefficient).permit(:year, :administrative_group_id, :coefficient)
    end
end
