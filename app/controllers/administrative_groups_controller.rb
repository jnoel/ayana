class AdministrativeGroupsController < ApplicationController
  include ApplicationHelper
  before_action :set_administrative_group, only: [:show, :edit, :update, :destroy]
  before_action :set_year, only: [:stats, :parameters]

  # GET /administrative_groups
  # GET /administrative_groups.json
  def index
    @administrative_groups = AdministrativeGroup.all
    @graph_nb_eleve = []
    @administrative_groups.each do |ag|
      @graph_nb_eleve << [ag.title, ag.number_of_students]
    end
  end

  def stats
    @administrative_groups = AdministrativeGroup.all
    @nb_open_days = OpenDay.where(year: @year).first

    @graph = []
    @administrative_groups.each do |group|
      graph_month = []
      (1..12).to_a.each do |month|
        mois = month_array_min_english[month-1]
        nb_open_days = @nb_open_days ? @nb_open_days[mois] : 0
        date = Date.parse("#{@year}-#{month}-01")+1.month-1.day
        nb_max = group.nb_students_at(date)*nb_open_days
        nb_prevue = group.nb_jours_prevus(date).round(0)
        nb_realisee = nb_max - group.nb_absences_at(month, @year)
        ecart = nb_realisee - nb_prevue
        pourc = nb_realisee*100/nb_max
        if (Date.parse("#{@year}-#{month}-1") + 1.month - 1.day) > Date.today
          graph_month << [month_array[month-1], 0]
        else
          graph_month << [month_array[month-1], ecart]
        end
      end
      @graph << {name: group.title, data: graph_month}
    end
  end

  def parameters
    @administrative_groups = AdministrativeGroup.all
    @open_days = OpenDay.where(year: @year).first
  end

  # GET /administrative_groups/1
  # GET /administrative_groups/1.json
  def show
    @students = Student.where(administrative_group_id: @administrative_group.id).order(:last_name, :first_name)
    @graph_age = {}
    @students.each do |student|
      @graph_age[student.age] = @graph_age[student.age].to_i + 1
    end
  end

  # GET /administrative_groups/new
  def new
    @administrative_group = AdministrativeGroup.new
  end

  # GET /administrative_groups/1/edit
  def edit
  end

  # POST /administrative_groups
  # POST /administrative_groups.json
  def create
    @administrative_group = AdministrativeGroup.new(administrative_group_params)

    respond_to do |format|
      if @administrative_group.save
        format.html { redirect_to @administrative_group, notice: 'Administrative group was successfully created.' }
        format.json { render :show, status: :created, location: @administrative_group }
      else
        format.html { render :new }
        format.json { render json: @administrative_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /administrative_groups/1
  # PATCH/PUT /administrative_groups/1.json
  def update
    respond_to do |format|
      if @administrative_group.update(administrative_group_params)
        format.html { redirect_to @administrative_group, notice: 'Administrative group was successfully updated.' }
        format.json { render :show, status: :ok, location: @administrative_group }
      else
        format.html { render :edit }
        format.json { render json: @administrative_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /administrative_groups/1
  # DELETE /administrative_groups/1.json
  def destroy
    @administrative_group.destroy
    respond_to do |format|
      format.html { redirect_to administrative_groups_url, notice: 'Administrative group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_administrative_group
      @administrative_group = AdministrativeGroup.find(params[:id])
    end

    def set_year
      @year = params["year"] ? params["year"].to_i : Date.today.year
      @year = Date.today.year if @year > Date.today.year
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def administrative_group_params
      params.require(:administrative_group).permit(:title, :description)
    end
end
