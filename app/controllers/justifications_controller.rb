class JustificationsController < ApplicationController

	# GET /justifications
  # GET /justifications.json
  def index
    @year = params["year"] ? params["year"].to_i : Date.today.year
    @year = Date.today.year if @year > Date.today.year
    date_deb = Date.parse("01/01/#{@year}")
    date_fin = Date.parse("31/12/#{@year}")
    @absences_injustifiees = Absence.includes(:student, :physical_group, :administrative_group).joins(:reason).where(date: date_deb..date_fin).where("reasons.title=?", "?").order("date DESC")
  end

end
