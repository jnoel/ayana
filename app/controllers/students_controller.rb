class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]
  before_action :set_groups, only: [:new, :create, :edit]

  # GET /students
  # GET /students.json
  def index
    @students = Student.order(:last_name, :first_name)
  end

  # GET /students/1
  # GET /students/1.json
  def show
    @absences_injustifiees = Absence.joins(:reason).where(student_id: @student.id).where("reasons.title=?", "?").order("date DESC")
  end

  # GET /students/new
  def new
    @student = Student.new
    @student.administrative_group_id = params["administrative_group"] if params["administrative_group"]
    @student.physical_group_id = params["physical_group"] if params["physical_group"]
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    def set_groups
      @administrative_groups = AdministrativeGroup.order(:title)
      @physical_groups = PhysicalGroup.order(:title)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:first_name, :last_name, :birth_date, :roomer, :physical_group_id, :administrative_group_id, :date_in, :secu_number, :sex, :caisse_id, :coef)
    end
end
