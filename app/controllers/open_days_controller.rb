class OpenDaysController < ApplicationController
  before_action :set_open_day, only: [:show, :edit, :update, :destroy]

  # GET /open_days
  # GET /open_days.json
  def index
    @open_days = OpenDay.all
  end

  # GET /open_days/1
  # GET /open_days/1.json
  def show
  end

  # GET /open_days/new
  def new
    @open_day = OpenDay.new
    if params["year"].to_i > 0
      @open_day.year = params["year"].to_i
    else
      redirect_to :back
    end
  end

  # GET /open_days/1/edit
  def edit
  end

  # POST /open_days
  # POST /open_days.json
  def create
    @open_day = OpenDay.new(open_day_params)

    respond_to do |format|
      if @open_day.save
        format.html { redirect_to "/administrative_groups/stats?year#{@open_day.year}", notice: 'Open day was successfully created.' }
        format.json { render :show, status: :created, location: @open_day }
      else
        format.html { render :new }
        format.json { render json: @open_day.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /open_days/1
  # PATCH/PUT /open_days/1.json
  def update
    respond_to do |format|
      if @open_day.update(open_day_params)
        format.html { redirect_to "/administrative_groups/stats?year#{@open_day.year}", notice: 'Open day was successfully updated.' }
        format.json { render :show, status: :ok, location: @open_day }
      else
        format.html { render :edit }
        format.json { render json: @open_day.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /open_days/1
  # DELETE /open_days/1.json
  def destroy
    @open_day.destroy
    respond_to do |format|
      format.html { redirect_to open_days_url, notice: 'Open day was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_open_day
      @open_day = OpenDay.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def open_day_params
      params.require(:open_day).permit(:year, :jan, :feb, :mar, :apr, :may, :jun, :jul, :aug, :sep, :oct, :nov, :dec)
    end
end
