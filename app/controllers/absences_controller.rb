class AbsencesController < ApplicationController
  before_action :set_absence, only: [:show, :edit, :update, :destroy]

  # GET /absences
  # GET /absences.json
  def index
    @absences = Absence.order(:title)
  end

  # GET /absences/1
  # GET /absences/1.json
  def show
  end

  # GET /absences/new
  def new
    @absence = Absence.new
    @student = nil
    if params["student_id"]
      begin
        @student = Student.find(params["student_id"])
        @absence.student_id = @student.id
        @absence.physical_group_id = @student.physical_group_id
        @absence.administrative_group_id = @student.physical_group.administrative_group_id
        @absence.night = @student.roomer
      rescue
      end
    end
    if params["date"]
      begin
        @absence.date = Date.parse(params["date"])
      rescue
        @absence.date = Date.today
      end
    end

    if params["hour"]=="N"
      @absence.night = true
    else
      @absence.morning = true
      @absence.afternoon = true
    end

    @reasons = Reason.where("id<>1").order(:title)
  end

  # GET /absences/1/edit
  def edit
    @student = @absence.student
  end

  # POST /absences
  # POST /absences.json
  def create
    @absence = Absence.new(absence_params)
    @absence.absence = @absence.reason.absence if @absence.reason
    url = "/?date=#{@absence.date};group=#{@absence.physical_group_id}"
    respond_to do |format|
      if @absence.reason_id==1
        format.html { redirect_to url, notice: 'Absence was not created because reason is :' + @absence.reason.description }
        format.json { render :show, status: :created, location: @absence }
      elsif @absence.save
        format.html { redirect_to url, notice: 'Absence was successfully created.' }
        format.json { render :show, status: :created, location: @absence }
      else
        format.html { render :new }
        format.json { render json: @absence.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /absences/1
  # PATCH/PUT /absences/1.json
  def update
    respond_to do |format|
      if @absence.update(absence_params)
        @absence.absence = @absence.reason.absence
        @absence.save
        url = "/?date=#{@absence.date};group=#{@absence.physical_group_id}"
        if @absence.reason_id==1
          @absence.destroy
          format.html { redirect_to url, notice: 'Absence was successfully destroyed.' }
          format.json { head :no_content }
        else
          format.html { redirect_to url, notice: 'Absence was successfully updated.' }
          format.json { render :show, status: :ok, location: @absence }
        end
      else
        format.html { render :edit }
        format.json { render json: @absence.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /absences/1
  # DELETE /absences/1.json
  def destroy
    @absence.destroy
    respond_to do |format|
      format.html { redirect_to absences_url, notice: 'Absence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_absence
      @absence = Absence.find(params[:id])
      @reasons = Reason.order(:id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def absence_params
      params.require(:absence).permit(:student_id, :physical_group_id, :administrative_group_id, :date, :morning, :afternoon, :night, :reason_id)
    end
end
