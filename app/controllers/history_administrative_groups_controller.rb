class HistoryAdministrativeGroupsController < ApplicationController
  before_action :set_history_administrative_group, only: [:show, :edit, :update, :destroy]
  before_action :set_groups, only: [:new, :edit]

  # GET /history_administrative_groups
  # GET /history_administrative_groups.json
  def index
    @history_administrative_groups = HistoryAdministrativeGroup.all
  end

  # GET /history_administrative_groups/1
  # GET /history_administrative_groups/1.json
  def show
  end

  # GET /history_administrative_groups/new
  def new
    @history_administrative_group = HistoryAdministrativeGroup.new
    @student = params["student"] ? Student.find(params["student"].to_i) : nil
    if @student
      @history_administrative_group.student_id = @student.id
      @history_administrative_group.administrative_group_id = @student.last_administrative_group.administrative_group_id
    end
    @history_administrative_group.date_in = Date.today
  end

  # GET /history_administrative_groups/1/edit
  def edit
  end

  # POST /history_administrative_groups
  # POST /history_administrative_groups.json
  def create
    @history_administrative_group = HistoryAdministrativeGroup.new(history_administrative_group_params)

    respond_to do |format|
      if @history_administrative_group.save
        format.html { redirect_to @history_administrative_group.student, notice: 'History administrative group was successfully created.' }
        format.json { render :show, status: :created, location: @history_administrative_group }
      else
        format.html { render :new }
        format.json { render json: @history_administrative_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /history_administrative_groups/1
  # PATCH/PUT /history_administrative_groups/1.json
  def update
    respond_to do |format|
      if @history_administrative_group.update(history_administrative_group_params)
        format.html { redirect_to @history_administrative_group, notice: 'History administrative group was successfully updated.' }
        format.json { render :show, status: :ok, location: @history_administrative_group }
      else
        format.html { render :edit }
        format.json { render json: @history_administrative_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /history_administrative_groups/1
  # DELETE /history_administrative_groups/1.json
  def destroy
    @history_administrative_group.destroy
    respond_to do |format|
      format.html { redirect_to history_administrative_groups_url, notice: 'History administrative group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_history_administrative_group
      @history_administrative_group = HistoryAdministrativeGroup.find(params[:id])
    end

    def set_groups
      @administrative_groups = AdministrativeGroup.order(:title)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def history_administrative_group_params
      params.require(:history_administrative_group).permit(:student_id, :administrative_group_id, :date_in, :date_out)
    end
end
