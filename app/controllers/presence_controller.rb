class PresenceController < ApplicationController

	# GET /presences
  # GET /presences.json
  def index
    @absences = Absence.all
    if params["date"]
      begin
        @date = Date.parse(params["date"])
        #@date = Date.today if @date > Date.today
      rescue
        @date = Date.today
      end
    else
      @date = Date.today
    end

    @group = params["group"] ? PhysicalGroup.find(params["group"]) : PhysicalGroup.first
    @students = @group.students_at(@date).order(:last_name, :first_name)
    @absences = Absence.includes(:reason).where(date: @date, physical_group_id: @group.id)
    @physical_groups = PhysicalGroup.all

  end

  # GET /today
  # GET /today.json
  def absents_du_jour
    @absents_du_jour = Absence.where(date: Date.today)
  end

  # GET /today_email
  def absents_du_jour_email
    AbsenceMailer.absents_du_jour.deliver_now
    redirect_to "/today"
  end

end
