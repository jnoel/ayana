class HistoryPhysicalGroupsController < ApplicationController
  before_action :set_history_physical_group, only: [:show, :edit, :update, :destroy]
  before_action :set_groups, only: [:new, :edit]

  # GET /history_physical_groups
  # GET /history_physical_groups.json
  def index
    @history_physical_groups = HistoryPhysicalGroup.all
    @history_physical_groups = @history_physical_groups.where(student_id: params["student"]) if params["student"]
    @history_physical_groups = @history_physical_groups.where(physical_group_id: params["group"]) if params["group"]
  end

  # GET /history_physical_groups/1
  # GET /history_physical_groups/1.json
  def show
  end

  # GET /history_physical_groups/new
  def new
    @history_physical_group = HistoryPhysicalGroup.new
    @student = params["student"] ? Student.find(params["student"].to_i) : nil
    if @student
      @history_physical_group.student_id = @student.id
      @history_physical_group.physical_group_id = @student.last_physical_group.physical_group_id
    end
    @history_physical_group.date_in = Date.today
  end

  # GET /history_physical_groups/1/edit
  def edit
  end

  # POST /history_physical_groups
  # POST /history_physical_groups.json
  def create
    @history_physical_group = HistoryPhysicalGroup.new(history_physical_group_params)

    respond_to do |format|
      if @history_physical_group.save
        format.html { redirect_to @history_physical_group.student, notice: 'History physical group was successfully created.' }
        format.json { render :show, status: :created, location: @history_physical_group }
      else
        format.html { render :new }
        format.json { render json: @history_physical_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /history_physical_groups/1
  # PATCH/PUT /history_physical_groups/1.json
  def update
    respond_to do |format|
      if @history_physical_group.update(history_physical_group_params)
        format.html { redirect_to @history_physical_group, notice: 'History physical group was successfully updated.' }
        format.json { render :show, status: :ok, location: @history_physical_group }
      else
        format.html { render :edit }
        format.json { render json: @history_physical_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /history_physical_groups/1
  # DELETE /history_physical_groups/1.json
  def destroy
    @history_physical_group.destroy
    respond_to do |format|
      format.html { redirect_to history_physical_groups_url, notice: 'History physical group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_history_physical_group
      @history_physical_group = HistoryPhysicalGroup.find(params[:id])
    end

    def set_groups
      @physical_groups = PhysicalGroup.order(:title)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def history_physical_group_params
      params.require(:history_physical_group).permit(:student_id, :physical_group_id, :date_in)
    end
end
