class PhysicalGroupsController < ApplicationController
  include ApplicationHelper
  before_action :set_physical_group, only: [:show, :edit, :update, :destroy, :absences]
  before_action :set_student, only: [:show]
  before_action :set_year, only: [:stats, :parameters]

  # GET /physical_groups
  # GET /physical_groups.json
  def index
    @physical_groups = PhysicalGroup.all
    @graph_nb_eleve = []
    @physical_groups.each do |ag|
      @graph_nb_eleve << [ag.title, ag.number_of_students]
    end
  end

  def stats
    @physical_groups = PhysicalGroup.all
    @nb_open_days = OpenDay.where(year: @year).first

    @graph = []
    @physical_groups.each do |group|
      graph_month = []
      (1..12).to_a.each do |month|
        mois = month_array_min_english[month-1]
        nb_open_days = @nb_open_days ? @nb_open_days[mois] : 0
        date = Date.parse("#{@year}-#{month}-01")+1.month-1.day
        nb_max = group.nb_students_at(date)*nb_open_days
        nb_prevue = group.nb_jours_prevus(date).round(0)
        nb_realisee = nb_max - group.nb_absences_at(month, @year)
        ecart = nb_realisee - nb_prevue
        pourc = nb_realisee*100/nb_max
        if (Date.parse("#{@year}-#{month}-1") + 1.month - 1.day) > Date.today
          graph_month << [month_array[month-1], 0]
        else
          graph_month << [month_array[month-1], ecart]
        end
      end
      @graph << {name: group.title, data: graph_month}
    end
  end

  def parameters
    @physical_groups = PhysicalGroup.all
    @open_days = OpenDay.where(year: @year).first
  end

  # GET /physical_groups/1
  # GET /physical_groups/1.json
  def show
    @graph_age = {}
    @students.each do |student|
      @graph_age[student.age] = @graph_age[student.age].to_i + 1
    end
  end

  # GET /physical_groups/new
  def new
    @physical_group = PhysicalGroup.new
    @physical_group.administrative_group_id = params["group"] if params["group"]
  end

  # GET /physical_groups/1/edit
  def edit

  end

  # GET /physical_groups/1/absences
  def absences
    @year = params["year"] ? params["year"].to_i : Date.today.year
    @students = @physical_group.students_at_year(@year)
    date_deb = Date.parse("01/01/#{@year}")
    date_fin = Date.parse("31/12/#{@year}")
    @absences = Absence.joins(:reason).where(physical_group_id: @physical_group.id, date: date_deb..date_fin, absence: true).group("reasons.description").count
  end

  # POST /physical_groups
  # POST /physical_groups.json
  def create
    @physical_group = PhysicalGroup.new(physical_group_params)

    respond_to do |format|
      if @physical_group.save
        format.html { redirect_to @physical_group, notice: 'Physical group was successfully created.' }
        format.json { render :show, status: :created, location: @physical_group }
      else
        format.html { render :new }
        format.json { render json: @physical_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /physical_groups/1
  # PATCH/PUT /physical_groups/1.json
  def update
    respond_to do |format|
      if @physical_group.update(physical_group_params)
        format.html { redirect_to @physical_group, notice: 'Physical group was successfully updated.' }
        format.json { render :show, status: :ok, location: @physical_group }
      else
        format.html { render :edit }
        format.json { render json: @physical_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /physical_groups/1
  # DELETE /physical_groups/1.json
  def destroy
    @physical_group.destroy
    respond_to do |format|
      format.html { redirect_to physical_groups_url, notice: 'Physical group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_physical_group
      @physical_group = PhysicalGroup.find(params[:id])
    end

    def set_year
      @year = params["year"] ? params["year"].to_i : Date.today.year
      @year = Date.today.year if @year > Date.today.year
    end

    def set_student
      @students = Student.where(physical_group_id: @physical_group.id).order(:last_name, :first_name)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def physical_group_params
      params.require(:physical_group).permit(:title, :description, :administrative_group_id)
    end
end
