class ApplicationMailer < ActionMailer::Base
  default from: "ayana@favron.org"
  layout 'mailer'
end
