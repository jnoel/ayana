class AbsenceMailer < ApplicationMailer
  def absents_du_jour
    @absents_du_jour = Absence.where(date: Date.today).order(:physical_group_id)
    mail(to: "mail@mithril.re", subject: 'Absents du jour')
  end
end
