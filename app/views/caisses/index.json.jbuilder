json.array!(@caisses) do |caiss|
  json.extract! caiss, :id, :title, :description
  json.url caiss_url(caiss, format: :json)
end
