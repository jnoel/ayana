json.array!(@physical_groups) do |physical_group|
  json.extract! physical_group, :id, :title, :description, :administrative_group_id
  json.url physical_group_url(physical_group, format: :json)
end
