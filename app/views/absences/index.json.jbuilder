json.array!(@absences) do |absence|
  json.extract! absence, :id, :student_id, :physical_group_id, :administrative_group_id, :date, :morning, :afternoon, :night, :reason_id
  json.url absence_url(absence, format: :json)
end
