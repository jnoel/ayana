json.array!(@open_days) do |open_day|
  json.extract! open_day, :id, :year, :jan, :fev, :mar, :avr, :mai, :jun, :jul, :aou, :sep, :oct, :nov, :dec
  json.url open_day_url(open_day, format: :json)
end
