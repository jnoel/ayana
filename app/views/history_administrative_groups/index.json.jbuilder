json.array!(@history_administrative_groups) do |history_administrative_group|
  json.extract! history_administrative_group, :id, :student_id, :administrative_group_id, :date_in, :date_out
  json.url history_administrative_group_url(history_administrative_group, format: :json)
end
