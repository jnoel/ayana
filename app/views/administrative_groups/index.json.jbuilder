json.array!(@administrative_groups) do |administrative_group|
  json.extract! administrative_group, :id, :title, :description
  json.url administrative_group_url(administrative_group, format: :json)
end
