json.array!(@history_physical_groups) do |history_physical_group|
  json.extract! history_physical_group, :id, :student_id, :physical_group_id, :date_in, :date_out
  json.url history_physical_group_url(history_physical_group, format: :json)
end
