json.array!(@group_coefficients) do |group_coefficient|
  json.extract! group_coefficient, :id, :year, :administratif_group_id, :coefficient
  json.url group_coefficient_url(group_coefficient, format: :json)
end
