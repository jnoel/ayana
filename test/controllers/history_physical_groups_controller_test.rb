require 'test_helper'

class HistoryPhysicalGroupsControllerTest < ActionController::TestCase
  setup do
    @history_physical_group = history_physical_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:history_physical_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create history_physical_group" do
    assert_difference('HistoryPhysicalGroup.count') do
      post :create, history_physical_group: { date_in: @history_physical_group.date_in, date_out: @history_physical_group.date_out, physical_group_id: @history_physical_group.physical_group_id, student_id: @history_physical_group.student_id }
    end

    assert_redirected_to history_physical_group_path(assigns(:history_physical_group))
  end

  test "should show history_physical_group" do
    get :show, id: @history_physical_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @history_physical_group
    assert_response :success
  end

  test "should update history_physical_group" do
    patch :update, id: @history_physical_group, history_physical_group: { date_in: @history_physical_group.date_in, date_out: @history_physical_group.date_out, physical_group_id: @history_physical_group.physical_group_id, student_id: @history_physical_group.student_id }
    assert_redirected_to history_physical_group_path(assigns(:history_physical_group))
  end

  test "should destroy history_physical_group" do
    assert_difference('HistoryPhysicalGroup.count', -1) do
      delete :destroy, id: @history_physical_group
    end

    assert_redirected_to history_physical_groups_path
  end
end
