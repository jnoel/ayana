require 'test_helper'

class PhysicalGroupsControllerTest < ActionController::TestCase
  setup do
    @physical_group = physical_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:physical_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create physical_group" do
    assert_difference('PhysicalGroup.count') do
      post :create, physical_group: { administrative_group_id: @physical_group.administrative_group_id, description: @physical_group.description, title: @physical_group.title }
    end

    assert_redirected_to physical_group_path(assigns(:physical_group))
  end

  test "should show physical_group" do
    get :show, id: @physical_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @physical_group
    assert_response :success
  end

  test "should update physical_group" do
    patch :update, id: @physical_group, physical_group: { administrative_group_id: @physical_group.administrative_group_id, description: @physical_group.description, title: @physical_group.title }
    assert_redirected_to physical_group_path(assigns(:physical_group))
  end

  test "should destroy physical_group" do
    assert_difference('PhysicalGroup.count', -1) do
      delete :destroy, id: @physical_group
    end

    assert_redirected_to physical_groups_path
  end
end
