require 'test_helper'

class OpenDaysControllerTest < ActionController::TestCase
  setup do
    @open_day = open_days(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:open_days)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create open_day" do
    assert_difference('OpenDay.count') do
      post :create, open_day: { aou: @open_day.aou, avr: @open_day.avr, dec: @open_day.dec, fev: @open_day.fev, jan: @open_day.jan, jul: @open_day.jul, jun: @open_day.jun, mai: @open_day.mai, mar: @open_day.mar, nov: @open_day.nov, oct: @open_day.oct, sep: @open_day.sep, year: @open_day.year }
    end

    assert_redirected_to open_day_path(assigns(:open_day))
  end

  test "should show open_day" do
    get :show, id: @open_day
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @open_day
    assert_response :success
  end

  test "should update open_day" do
    patch :update, id: @open_day, open_day: { aou: @open_day.aou, avr: @open_day.avr, dec: @open_day.dec, fev: @open_day.fev, jan: @open_day.jan, jul: @open_day.jul, jun: @open_day.jun, mai: @open_day.mai, mar: @open_day.mar, nov: @open_day.nov, oct: @open_day.oct, sep: @open_day.sep, year: @open_day.year }
    assert_redirected_to open_day_path(assigns(:open_day))
  end

  test "should destroy open_day" do
    assert_difference('OpenDay.count', -1) do
      delete :destroy, id: @open_day
    end

    assert_redirected_to open_days_path
  end
end
