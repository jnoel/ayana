require 'test_helper'

class GroupCoefficientsControllerTest < ActionController::TestCase
  setup do
    @group_coefficient = group_coefficients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:group_coefficients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create group_coefficient" do
    assert_difference('GroupCoefficient.count') do
      post :create, group_coefficient: { administratif_group_id: @group_coefficient.administratif_group_id, coefficient: @group_coefficient.coefficient, year: @group_coefficient.year }
    end

    assert_redirected_to group_coefficient_path(assigns(:group_coefficient))
  end

  test "should show group_coefficient" do
    get :show, id: @group_coefficient
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @group_coefficient
    assert_response :success
  end

  test "should update group_coefficient" do
    patch :update, id: @group_coefficient, group_coefficient: { administratif_group_id: @group_coefficient.administratif_group_id, coefficient: @group_coefficient.coefficient, year: @group_coefficient.year }
    assert_redirected_to group_coefficient_path(assigns(:group_coefficient))
  end

  test "should destroy group_coefficient" do
    assert_difference('GroupCoefficient.count', -1) do
      delete :destroy, id: @group_coefficient
    end

    assert_redirected_to group_coefficients_path
  end
end
