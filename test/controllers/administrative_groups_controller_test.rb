require 'test_helper'

class AdministrativeGroupsControllerTest < ActionController::TestCase
  setup do
    @administrative_group = administrative_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:administrative_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create administrative_group" do
    assert_difference('AdministrativeGroup.count') do
      post :create, administrative_group: { description: @administrative_group.description, title: @administrative_group.title }
    end

    assert_redirected_to administrative_group_path(assigns(:administrative_group))
  end

  test "should show administrative_group" do
    get :show, id: @administrative_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @administrative_group
    assert_response :success
  end

  test "should update administrative_group" do
    patch :update, id: @administrative_group, administrative_group: { description: @administrative_group.description, title: @administrative_group.title }
    assert_redirected_to administrative_group_path(assigns(:administrative_group))
  end

  test "should destroy administrative_group" do
    assert_difference('AdministrativeGroup.count', -1) do
      delete :destroy, id: @administrative_group
    end

    assert_redirected_to administrative_groups_path
  end
end
