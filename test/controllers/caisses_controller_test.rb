require 'test_helper'

class CaissesControllerTest < ActionController::TestCase
  setup do
    @caiss = caisses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:caisses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create caiss" do
    assert_difference('Caisse.count') do
      post :create, caiss: { description: @caiss.description, title: @caiss.title }
    end

    assert_redirected_to caiss_path(assigns(:caiss))
  end

  test "should show caiss" do
    get :show, id: @caiss
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @caiss
    assert_response :success
  end

  test "should update caiss" do
    patch :update, id: @caiss, caiss: { description: @caiss.description, title: @caiss.title }
    assert_redirected_to caiss_path(assigns(:caiss))
  end

  test "should destroy caiss" do
    assert_difference('Caisse.count', -1) do
      delete :destroy, id: @caiss
    end

    assert_redirected_to caisses_path
  end
end
