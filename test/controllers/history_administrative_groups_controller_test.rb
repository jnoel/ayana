require 'test_helper'

class HistoryAdministrativeGroupsControllerTest < ActionController::TestCase
  setup do
    @history_administrative_group = history_administrative_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:history_administrative_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create history_administrative_group" do
    assert_difference('HistoryAdministrativeGroup.count') do
      post :create, history_administrative_group: { administrative_group_id: @history_administrative_group.administrative_group_id, date_in: @history_administrative_group.date_in, date_out: @history_administrative_group.date_out, student_id: @history_administrative_group.student_id }
    end

    assert_redirected_to history_administrative_group_path(assigns(:history_administrative_group))
  end

  test "should show history_administrative_group" do
    get :show, id: @history_administrative_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @history_administrative_group
    assert_response :success
  end

  test "should update history_administrative_group" do
    patch :update, id: @history_administrative_group, history_administrative_group: { administrative_group_id: @history_administrative_group.administrative_group_id, date_in: @history_administrative_group.date_in, date_out: @history_administrative_group.date_out, student_id: @history_administrative_group.student_id }
    assert_redirected_to history_administrative_group_path(assigns(:history_administrative_group))
  end

  test "should destroy history_administrative_group" do
    assert_difference('HistoryAdministrativeGroup.count', -1) do
      delete :destroy, id: @history_administrative_group
    end

    assert_redirected_to history_administrative_groups_path
  end
end
