Rails.application.routes.draw do
  resources :users
  resources :caisses
  resources :group_coefficients
  resources :open_days
  resources :history_administrative_groups
  resources :history_physical_groups
  resources :absences
  resources :reasons
  resources :students
  get 'physical_groups/stats' => 'physical_groups#stats'
  get 'physical_groups/parameters' => 'group_coefficients#index'
  resources :physical_groups
  get 'physical_groups/:id/absences' => 'physical_groups#absences'
  get 'administrative_groups/stats' => 'administrative_groups#stats'
  get 'administrative_groups/parameters' => 'group_coefficients#index'
  get '/justifications' => 'justifications#index'
  resources :administrative_groups
  root 'presence#index'
  get 'about' => 'about#index'

  get 'today' => 'presence#absents_du_jour'
  get 'today_email' => 'presence#absents_du_jour_email'

  resource :user_sessions, only: [:create, :new, :destroy]

  get 'logout' => 'user_sessions#destroy', :as => :logout
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
