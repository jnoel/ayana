# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160324160614) do

  create_table "absences", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "physical_group_id"
    t.integer  "administrative_group_id"
    t.date     "date"
    t.integer  "reason_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.boolean  "morning"
    t.boolean  "afternoon"
    t.boolean  "night"
    t.boolean  "absence"
  end

  add_index "absences", ["administrative_group_id"], name: "index_absences_on_administrative_group_id"
  add_index "absences", ["physical_group_id"], name: "index_absences_on_physical_group_id"
  add_index "absences", ["reason_id"], name: "index_absences_on_reason_id"
  add_index "absences", ["student_id"], name: "index_absences_on_student_id"

  create_table "administrative_groups", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "caisses", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "group_coefficients", force: :cascade do |t|
    t.integer  "year"
    t.integer  "administrative_group_id"
    t.float    "coefficient"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "group_coefficients", ["administrative_group_id"], name: "index_group_coefficients_on_administrative_group_id"

  create_table "history_administrative_groups", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "administrative_group_id"
    t.date     "date_in"
    t.date     "date_out"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "history_administrative_groups", ["administrative_group_id"], name: "index_history_administrative_groups_on_administrative_group_id"
  add_index "history_administrative_groups", ["student_id"], name: "index_history_administrative_groups_on_student_id"

  create_table "history_physical_groups", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "physical_group_id"
    t.date     "date_in"
    t.date     "date_out"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "history_physical_groups", ["physical_group_id"], name: "index_history_physical_groups_on_physical_group_id"
  add_index "history_physical_groups", ["student_id"], name: "index_history_physical_groups_on_student_id"

  create_table "open_days", force: :cascade do |t|
    t.integer  "year"
    t.integer  "jan"
    t.integer  "feb"
    t.integer  "mar"
    t.integer  "apr"
    t.integer  "may"
    t.integer  "jun"
    t.integer  "jul"
    t.integer  "aug"
    t.integer  "sep"
    t.integer  "oct"
    t.integer  "nov"
    t.integer  "dec"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "physical_groups", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "reasons", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "absence",     default: true
  end

  create_table "students", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "birth_date"
    t.integer  "physical_group_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.boolean  "roomer"
    t.integer  "administrative_group_id"
    t.date     "date_in"
    t.string   "sex"
    t.string   "secu_number"
    t.integer  "caisse_id"
    t.float    "coef"
  end

  add_index "students", ["administrative_group_id"], name: "index_students_on_administrative_group_id"
  add_index "students", ["caisse_id"], name: "index_students_on_caisse_id"
  add_index "students", ["physical_group_id"], name: "index_students_on_physical_group_id"

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.datetime "last_login_at"
    t.datetime "current_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.integer  "login_count",         default: 0, null: false
    t.integer  "failed_login_count",  default: 0, null: false
    t.datetime "last_request_at"
    t.string   "name"
    t.string   "persistence_token"
    t.string   "single_access_token"
    t.string   "perishable_token"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

end
