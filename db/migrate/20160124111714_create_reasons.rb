class CreateReasons < ActiveRecord::Migration
  def change
    create_table :reasons do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end

    Reason.create title:"*", description: "Présent"
    Reason.create title:"IS", description: "Inclusion Scolaire"
    Reason.create title:"STA", description: "Mise en stage"
    Reason.create title:"UED", description: "UED"
    Reason.create title:"FA", description: "Famille d'accueil"
    Reason.create title:"?", description: "Absence à justifier"
    Reason.create title:"HOS", description: "Hospitalisation"
    Reason.create title:"MAL", description: "Maladie"
    Reason.create title:"EF", description: "Evènement Familial"
    Reason.create title:"AT", description: "Accueil Temporaire"
    Reason.create title:"RH", description: "Fermeture école"
    Reason.create title:"RDV", description: "Rendez-vous médicaux"

  end
end
