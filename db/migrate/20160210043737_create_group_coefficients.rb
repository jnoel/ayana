class CreateGroupCoefficients < ActiveRecord::Migration
  def change
    create_table :group_coefficients do |t|
      t.integer :year
      t.belongs_to :administrative_group, index: true, foreign_key: true
      t.float :coefficient

      t.timestamps null: false
    end
  end
end
