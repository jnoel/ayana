class CreateOpenDays < ActiveRecord::Migration
  def change
    create_table :open_days do |t|
      t.integer :year
      t.integer :jan
      t.integer :feb
      t.integer :mar
      t.integer :apr
      t.integer :may
      t.integer :jun
      t.integer :jul
      t.integer :aug
      t.integer :sep
      t.integer :oct
      t.integer :nov
      t.integer :dec

      t.timestamps null: false
    end

    OpenDay.create year: 2016, jan: 12, feb: 21, mar: 18, apr: 17, may: 19, jun: 14, jul: 5, aug: 8, sep: 19, oct: 18, nov: 19, dec:12
  end
end
