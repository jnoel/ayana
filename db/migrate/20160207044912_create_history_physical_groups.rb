class CreateHistoryPhysicalGroups < ActiveRecord::Migration
  def change
    create_table :history_physical_groups do |t|
      t.belongs_to :student, index: true, foreign_key: true
      t.belongs_to :physical_group, index: true, foreign_key: true
      t.date :date_in
      t.date :date_out

      t.timestamps null: false
    end
  end
end
