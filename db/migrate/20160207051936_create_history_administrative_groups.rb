class CreateHistoryAdministrativeGroups < ActiveRecord::Migration
  def change
    create_table :history_administrative_groups do |t|
      t.belongs_to :student, index: true, foreign_key: true
      t.belongs_to :administrative_group, index: true, foreign_key: true
      t.date :date_in
      t.date :date_out

      t.timestamps null: false
    end
  end
end
