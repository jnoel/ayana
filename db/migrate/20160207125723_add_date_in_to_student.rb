class AddDateInToStudent < ActiveRecord::Migration
  def change
    add_column :students, :date_in, :date
  end
end
