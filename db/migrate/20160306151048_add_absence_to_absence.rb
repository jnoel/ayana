class AddAbsenceToAbsence < ActiveRecord::Migration
  def change
    add_column :absences, :absence, :boolean
  end
end
