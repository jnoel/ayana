class CreateAdministrativeGroups < ActiveRecord::Migration
  def change
    create_table :administrative_groups do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
