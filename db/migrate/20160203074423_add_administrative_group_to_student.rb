class AddAdministrativeGroupToStudent < ActiveRecord::Migration
  def change
    add_reference :students, :administrative_group, index: true, foreign_key: true
  end
end
