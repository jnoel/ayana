class AddAbsenceToReason < ActiveRecord::Migration
  def change
    add_column :reasons, :absence, :boolean, default: true
  end
end
