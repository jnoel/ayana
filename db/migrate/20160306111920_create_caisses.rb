class CreateCaisses < ActiveRecord::Migration
  def change
    create_table :caisses do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
