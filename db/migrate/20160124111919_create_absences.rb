class CreateAbsences < ActiveRecord::Migration
  def change
    create_table :absences do |t|
      t.references :student, index: true, foreign_key: true
      t.belongs_to :physical_group, index: true, foreign_key: true
      t.belongs_to :administrative_group, index: true, foreign_key: true
      t.date :date
      t.string :hour
      t.belongs_to :reason, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
