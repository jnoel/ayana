class RemoveAdministrativeGroupFromPhysicalGroup < ActiveRecord::Migration
  def change
    remove_reference :physical_groups, :administrative_group, index: true, foreign_key: true
  end
end
