class AddParametersToStudent < ActiveRecord::Migration
  def change
    add_column :students, :sex, :string
    add_column :students, :secu_number, :string
    add_reference :students, :caisse, index: true, foreign_key: true
  end
end
