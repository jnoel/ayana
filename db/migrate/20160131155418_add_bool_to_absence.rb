class AddBoolToAbsence < ActiveRecord::Migration
  def change
    add_column :absences, :morning, :boolean
    add_column :absences, :afternoon, :boolean
    add_column :absences, :night, :boolean
    remove_column :absences, :hour
  end
end
