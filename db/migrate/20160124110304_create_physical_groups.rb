class CreatePhysicalGroups < ActiveRecord::Migration
  def change
    create_table :physical_groups do |t|
      t.string :title
      t.text :description
      t.belongs_to :administrative_group, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
